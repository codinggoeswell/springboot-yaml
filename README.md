# 개요
이 프로젝트는 YAML 파일을 사용하는 방법과 Spring Profile을 적용하는 방법을 테스트하기 위한 것이다. 

    
# 실행
브라우저에서 다음과 같이 입력한다. 화면에는 아무것도 출력되지 않는다. Eclipse의 Console 출력을 확인한다. 

```
http://localhost:8080/test 
```




# Spring Profiles

어떤  profile을  사용할지(active)를 정하기 위해서 spring.profiles.active 환경 property를 사용할 수 있다. 하나 이상의 프로파일을 사용하려면 콤마로 구분한다. 

applicaiton.properties에 포함할 수 있다. 

```
spring.profiles.active=dev,hsqldb 
```

command line에서도 명시할 수 있다.

```
 java -jar -Dspring.profiles.active=dev yourproject.jar 
 ```


## 어노테이션 사용
어떤 환경에서 가능하게 할 지 @Configuration 또는 @Component은 로드될 때 제약을 두기 위해 @Profile을 사용할 수 있다. 

```java
@Configuration
@Profile("production")
public class ProductionConfiguration {

    // ...

}

```


application.properties에서 active profiles을 명시할 수 있고 그런 다음 command line에서 바꿀 수 있다. 

spring.profiles.include는 무조건 적으로 active profiles을 추가하는데 사용될 수 있다.  예를들어, --spring.profiles.active=prod를 사용한 다음의 properties를 가진 application이 실행될 때, proddb와 prodmq profiles는 활성화 될 수 있다. 

```
---
my.property: fromyamlfile
---
spring.profiles: prod
spring.profiles.include: proddb,prodmq
```

## @Value 어노테이션 사용
Property 값들은 @Value 어노테이션을 사용하여 beans들에 직접적으로 주입될 수 있다. 


##  Profile specific properties
application.properties 파일 이외에, profile specific properties이 naming convention을 사용하여 정의될 수 있다. 프로파일을 여러 파일에 나누어 정의할 수 있다는 의미다. 

**단, yaml 파일을 사용할 때는 동작하지 않는다. properties 파일을 사용할 때만 가능하다.**


application-{profile}.properties


특정 프로파일 properties는 표준 application.properties와 같은 위치에서 로드된다. 


## Placeholders in properties
application.properties 내의 값들은 존재하는 Environment를 통해서 필터링 된다.  그것들이 사용될 때 이전에 이미 정의된 것을 뒤에서 참조될 수 있다. 앞에서 정의한 프러퍼티는 뒤에서 그 값을 사용할 수 있다. 
```
app.name=MyApp
app.description=${app.name} is a Spring Boot application
```


## Multi-profile YAML documents
하나의 파일에 여러개의 profile-specific YAML 문서들을 정의할 수 있다. spring.profiles를 사용한다. 

```
server:
    address: 192.168.1.100
---
spring:
    profiles: development
server:
    address: 127.0.0.1
---
spring:
    profiles: production
server:
    address: 192.168.1.120
```

위의 예에서, development profiles가 active이면  server.address는 127.0.0.1일 것이다.  development와 production profiles이 enabled 되지 않으면
192.168.1.100이 될 것이다. 


## YAML shortcomings
YAML 파일은 @PropertySource annotation을 통해 로드되지 않는다.  사용하고 싶으면 대신에 properties 파일을 사용한다. 


## @ConfigurationProperties
@Value("${property}")를 사용하는 것은 때로는 다루기 힘들 수도 있는데
@ConfigurationProperties를 사용할 수도 있다. 


## @EnableConfigurationProperties
@EnableConfigurationProperties 어노테이션이 @Configuration에 적용될 때
@configurationProperties으로 주석을 붙인 어떤 빈이든 
자동적으로 Environment properties로부터 설정될 수 있다. 



# 참조
https://docs.spring.io/spring-boot/docs/1.1.x/reference/html/boot-features-external-config.html
https://docs.spring.io/spring-boot/docs/1.1.x/reference/html/boot-features-profiles.html

    

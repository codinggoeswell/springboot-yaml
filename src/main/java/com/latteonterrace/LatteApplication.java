package com.latteonterrace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.AbstractEnvironment;

/**
 * profile을 적용하려면
 * 실행할 때 -Dspring.profiles.active=dev 와 같이 사용
 *    java -jar -Dspring.profiles.active=dev ***.jar 
 *
 */
@SpringBootApplication(scanBasePackages = { "com.latteonterrace" })
public class LatteApplication {
	public static void main(String[] args) {
		// Spring Profile을 설정하기 위한 값
		System.setProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME, "dev");
		String env = System.getProperty(AbstractEnvironment.ACTIVE_PROFILES_PROPERTY_NAME);
		System.out.println("$$$$$$ environment:" + env);
		SpringApplication.run(LatteApplication.class, args);
		// Applicion 실행은 아래의 방법으로도 가능함 
//		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(LatteApplication.class)
//				.profiles(env)
//				.run(args);
		
	}
}///~

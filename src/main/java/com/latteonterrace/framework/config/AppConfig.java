package com.latteonterrace.framework.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.latteonterrace.demo.DemoBean;

/**
 * application.xml은 src/main/resources 아래에 두어야 함 
 * @author Sanghyeon,Kim(sanghyun@naonsoft.com)
 *
 */
@Configuration
//@ConfigurationProperties  이 어노테이션을 사용할 필요는 없음
//@PropertySource("classpath:application.properties")  이 어노테이션을 사용할 필요는 없음
public class AppConfig {
	
 
	/** 
	 * {@literal}@Value 어노테이션을 사용하여 propery의 value를 주입한다. 
	 */
	@Value("${spring.freemarker.suffix}")
	private String suffix;
	
	public AppConfig() {
		System.out.println(">>>>>>> AppConfig created.");
		// 생성자에서는 주입이 안되어서 사용할 수 없음
		System.out.println(">>>>>>>>" + suffix);
	}
	
	@Bean
	public DemoBean makeBean() {
		// Bean을 생성할 때에는 프로퍼티가 주입이 되어서 정상적으로 값이 출력된다. 
		System.out.println("####" + suffix);
		return new DemoBean();
	}

}

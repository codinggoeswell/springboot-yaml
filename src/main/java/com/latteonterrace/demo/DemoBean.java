package com.latteonterrace.demo;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * application.yml의 속성값을 담을 클래스이다. 
 *  
 * {@literal}@ConfigurationProperties의 prefix 속성을 사용하면 
 * {@literal}@Value 어노테이션을 사용하지 않고 Bean 클래스의 필드에 자동으로 
 * applicaion.yml의 속성값을 주입할 수 있다. 
 *
 */
@Component
@ConfigurationProperties(prefix="author")
public class DemoBean {

	/** application.yml의 author.name 프러퍼티에 대응. */ 
	private String name;
	/** application.yml의 author.email 프러퍼티에 대응 */ 
	private String email;
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	} 
	
}

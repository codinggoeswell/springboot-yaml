package com.latteonterrace.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Demo용 Controller이다.  
 *
 */
@Controller
public class DemoController {

	@Autowired
	private DemoService demoService; 
	
	@RequestMapping(value="/test")
	public void test(HttpServletRequest req, HttpServletResponse res) {
		System.out.println("Environment.getProperty() 테스트 결과: " + this.demoService.getName());
	}
}
package com.latteonterrace.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *  Demo용 Service Class이다.  
 *
 */
@Service
//@EnableConfigurationProperties  이 어노테이션은 사용할 필요가 없음
public class DemoService {
	
	@Autowired
	private Environment env; 
	
	/** application.yml 파일의 프퍼퍼티 값 주입 테스트용 */
	@Value("${test.prop}")
	private String prop;
	
	/** application.yml 파일의 프로파일의 값 주입 테스트용 */ 
	@Value("${dev.environment}")
	private String environment;
	
	@Autowired
	private DemoBean demoBean; 

	public String getName() {
		System.out.println(">>>> testProp:" + prop);
		System.out.println(">>>> environment:" + environment);
		System.out.println("@ConfigurationProperties Test: " + demoBean.getEmail());
		// Environment.getProperty()를 사용하여 applicion.yml 속성값을 구할 수 있음.
		return env.getProperty("spring.freemarker.suffix");  
	}//:
	
}
